<!DOCTYPE html>
<html>
    <!--<script src="jquery-1.10.1.min.js"></script>-->
    <body>
      <!-- 1. The <iframe> (and video player) will replace this <div> tag. -->
        <iframe id="player" type="text/html" width="640" height="390"
                src="https://www.youtube.com/embed/pxe9yzpeYvA?enablejsapi=1"
                frameborder="0"></iframe>

        <script>
            // 2. This code loads the IFrame Player API code asynchronously.
             var player;
             
            var tag = document.createElement('script');
            tag.src = "https://www.youtube.com/iframe_api";
            var firstScriptTag = document.getElementsByTagName('script')[0];
            firstScriptTag.parentNode.insertBefore(tag, firstScriptTag);

            // 3. This function creates an <iframe> (and YouTube player)
            //    after the API code downloads.
           
            function onYouTubeIframeAPIReady() {
                player = new YT.Player('player', {
                    events: {
                        'onReady': onPlayerReady,
                        'onStateChange': onPlayerStateChange
                    }
                });
            }


            // 4. The API will call this function when the video player is ready.
            function onPlayerReady(event) {
                event.target.playVideo();
                console.log('player ready');
            }

            // 5. The API calls this function when the player's state changes.
            //    The function indicates that when playing a video (state=1),
            //    the player should play for six seconds and then stop.
            var done = false;
            function onPlayerStateChange(event) {
                console.log("player state changed");
                if (event.data == YT.PlayerState.PLAYING && !done) {
                    setTimeout(stopVideo, 6000);
                    done = true;
                }
            }
            function stopVideo() {
                player.stopVideo();
                console.log("stop video");
            }
        </script>
    </body>
</html>